
## Bootproject React JS website

This is a website made in React JS that features zone management. It makes layout and dynamic rendering an easy thing todo. See zones below.
All modules and content is loaded from a service api. (example included in services)

### Install
    git clone https://rickardmagnusson@bitbucket.org/rickardmagnusson/learningreactv2.git
    npm install
    npm run start
    npm run build (Build production scripts) 

### Features ###

    Zone element:
    <Zone zone="Content" match={match} />
    A zone represents an area where to put modules. A zone can contain multiple modules. All defined in service.
    The service contains just regular JSON.    

    @Params:
    zone (Required): The name of the zone.
    match (Required): Passes the arguments to components iterated in this zone.
   

### Templates:
    By define your templates in service(json api) you can serve templates defined in your project.
    Of course can you create any template that siuts your need, Below is just an example.
    The ({ match }) is "not optional" if you planning to use Routes thats should be passed to sub components.
    You can create a named zones as long as you use them in your app and is set in page routes.
    Example:

    const HomeTemplate = ({ match }) => {
    return (
        <section>
            <div className="container">
                <Nav title="Bootproject" />
                <Intro name="Bootproject" />
                <Zone zone="Content" match={match} />
                <Zone zone="Timeline" match={match} />
                <Zone zone="AfterContent" match={match} />
                <Zone zone="AboutContent" match={match} />
                <Footer />
            </div>
        </section>
        );
    }; 
    export { HomeTemplate };

### Modules
    Modules created:
    Topics
    ContentItem
    Nav
    Timeline
    
    Static modules
    Footer
    Intro
    

### Service
    Example route:
    Modules in Zone Content for the route of "/" (the start page)
    It has 2 modules, ContentItem and Timeline
     Routes: [
        {
            id: 1,
            title: "Start",
            path: "/",
            tmpl: "HomeTemplate",
            component: "HomeTemplate",
            exact: true,
            zones: [
                {
                    id: 1,
                    pageId: 1,
                    name: "Content", 
                    modules: [ 
                        {
                            id: 1,
                            pageId: 1,
                            name: "ContentItem"
                        },
                        {
                            id: 2,
                            pageId: 1,
                            name: "Timeline"
                        }
                    ]
                },{
                   id: 2,
                    pageId: 1,
                    name: "AfterContent", 
                    modules: [... 
                }
        }]



import React from 'react';
import { Switch, BrowserRouter as Router, Route } from 'react-router-dom';
import Api from "./services";
import Modules from "./utils";

class App extends React.Component {
    render() {
        return (
            <Router>
                <a href="#top" name="top" id="top" ref="top"></a>
                <Switch>
                    {Api.Routes.map(r => <Route key={r.path} path={r.path} exact={r.exact} component={Modules[r.component]} />)}
                </Switch>
            </Router>
        );
    }
}

export default App;
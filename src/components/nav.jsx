﻿import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';
import { Api } from "../services";

/**
    Navigation component (Top level)
 */
class Nav extends Component {

    constructor() {
        super();
        this.currentClass = this.currentClass.bind(this);
        this.state = {
            error: null,
            isLoaded: true
        };
    }

    currentClass(path) {

        var url = document.location.pathname;
        var parts = url.split("/");
        if (url.split("/").length > 2)
            if (path === "/" + parts[1])
                return "nav-link text-dark active";

        return path === document.location.pathname ? "nav-link text-dark active" : "nav-link text-dark";
    }

    renderClick(path) {
        //try {
        //    var ga = window.ga;
        //    ga('set', 'page', path);
        //    ga('send', 'pageview');
        //} catch (e) {
        //    console.log("Error: " + path);
        //}
    }

    render() {
        const { error, isLoaded } = this.state;

        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div><img src="img/react-spinner.gif" /></div>;
        } else {

            return (
                <nav className="navbar navbar-expand-lg top">
                    <Link className="navbar-brand" to="/"><img src="/img/logo.svg" alt="Logo" className="brand" /> {this.props.title}</Link>

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsCollapse" aria-controls="navbarsCollapse" aria-expanded="false" aria-label="Toggle navigation">
                        <span><i className="fa fa-bars fa-1x" /></span>
                    </button>

               

                    <div className="navbar-collapse collapse" id="navbarsCollapse">
                        <ul className="navbar-nav ml-auto">
                            {Api.Routes.map(page =>
                                <li key={page.id}><Link key={page.path} className={this.currentClass(page.path)} exact={page.exact.toString()} to={page.path} onClick={() => this.renderClick(page.path)}>{page.title}</Link></li>
                            )}
                        </ul>
                    </div>
                </nav>
            );
        }
    }
}

export default Nav;
export { Nav };
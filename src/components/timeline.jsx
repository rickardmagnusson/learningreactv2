﻿import React from 'react';
import { Api } from "../services";

/**
    Timeline on startpage
 */
const Timeline = ({ zone }) => {

    /** Fetch the dataitems from current page*/
    var data = Api.TimelineContent.items.filter(p => p.path === document.location.pathname && p.zone === zone);
    var html = "";

    for (var i in data) {
        var item = data[i];

        /** Current content item*/
        var contents = "";
        var header = "";

        if (item.list.length > 0) {
            var list = item.list;
            contents += `<header><div><h2>` + item.title + `</h2><h4>` + item.content + `</h4></div></header>`;
            for (var a in list) {
                contents += "<div class='timeline-point'><div class='timeline-icon'></div><div class='timeline-block'><div class='timeline-content'><h3>" + list[a].header + "</h3><p>" + list[a].content + "</p></div></div></div>";
            }
        }

        var c = `<div class="sectionroot">
                <section>
                    <div class="sharedSection">
                        <div class="sectioncontent">
                            <div class="timeliner">
                           `+ header + `
                           `+ contents + `
                            </div>
                        </div>
                    </div>
                </section>
            </div>`;
        html += c;
    }
    var obj = { __html: html };
    return (<span dangerouslySetInnerHTML={obj} />);
}

export default Timeline;
﻿/* eslint-disable */
import React, { Component } from "react";
import ReactDOM from "react-dom";
import { Route, Link } from 'react-router-dom';
import { Api } from '../services';


// eslint-disable-next-line
const Post = ({ post }) => {

    var html = { __html: post.content };

    return (<div>
        <h1>{post.title}</h1>
        <span dangerouslySetInnerHTML={html} />
    </div>);
};


/**
 Project topics
 */
class Topics extends Component {
    constructor(props) {
        super(props);

        this.state = {
            error: null,
            isLoaded: true,
            match: {}
        };
        this.currentClass = this.currentClass.bind(this);
    }

    componentDidUpdate() {
        let hash = document.location.hash.replace('#', '');
        if (hash) {
            let node = ReactDOM.findDOMNode(this.refs[hash]);
            if (node) {
                node.scrollIntoView();
            }
        }
    }

    currentClass(path) {
        var url = document.location.pathname;
        var parts = url.split("/");
        var part = url + parts[0];
        if (url === part)
            return "nav-link active";

        return path === document.location.pathname ? "nav-link active" : "nav-link";
    }

    renderClick(path) {
        //try {
        //    var ga = window.ga;
        //    ga('set', 'page', path);
        //    ga('send', 'pageview');
        //} catch (e) {
        //    console.log("Error: " + path);
        //}
    }

    render() {

       const { isLoaded } = this.state;

        if (!isLoaded) {
            return <div><img src="img/react-spinner.gif" /></div>;
        } else {

            return (
                <div className="sectionroot">
                    <section>
                        <div className="sharedSection">
                            <div className="sectioncontent">
                                <div>
                                    <div className="row" >
                                        <div className="col-lg-9">
                                           
                                            <Route path={`${this.props.match.path}#top`} exact render={({ match }) => ( //Default if no match
                                                <Post post={Api.TopicContent.topic.topics.map(p => p)[0]} />
                                            )} />

                                            <Route path={`${this.props.match.path}/:id`} render={({ match }) => ( //Map match(id) to post
                                                <Post post={Api.TopicContent.topic.topics.find(p => p.path === (match.params.id === "" ? "" : match.params.id))} />
                                            )} />
                                        </div>
                                        <div className="col-lg-3 sidebar timeliner"><h4>Projects</h4>
                                            {Api.TopicContent.topic.topics.map(t => <div key={"tm" + t.id} className='timeline-point'><div className='timeline-icon'></div><div className='timeline-block'><Link key={t.id} className={this.currentClass(t.path)} to={`${this.props.match.path}/${t.path}#top`}>{t.title}</Link></div></div>)}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            );
        }
    }
}

export default Topics;


﻿import React from 'react';
import { Api } from "../../services";
import ContentItem from "../../components/contentitem";
import Modules from "../../utils";

/**
 @param Object match, The route to match
 @param Object zone, the zone to map for this element
 */
const Zone = ({ match, zone }) => {

    /**
        @param Array zones
     */
    function createZoneElements(zones) {
        var elements = [];
        
        for (var item in zones) {
            for (var i = 0; i < zones[item].length; i++) {
                if (zones[item][i].name === zone) {
                    const modules = zones[item][i].modules.map(m => m);
                    for (var m in modules) {
                        elements.push(modules[m]);
                    }
                }
            }
        }
        return elements;
    }

    var page = Api.Routes.filter(p => p.path === match.path);
    var elements = createZoneElements(page.map(r => r.zones));

    return elements.map(r => React.createElement(Modules[r.name], { key: r.id, match: match, zone: zone }));
};

export { Zone };
export default Zone;
﻿import React from "react";

const Footer = () => {
    return (
        <footer>
            <section className="sharedSection">
                <div className="sectioncontent">
                    <ul className="shared_grid">
                        <li>
                            <img src="/img/user.white.png" alt="multi" className="mini" /> <h2>About</h2>
                            <p>The Swede(me), Rickard Magnusson a Senior fullstack developer, currently a consultant working in Norway.</p>
                        </li>
                        <li>
                            <h2>Legal</h2>
                            <p>Feel free to download this site on bitbucket and use it as you like.</p>
                        </li>
                        <li>
                            <h2><img src="/img/logo.svg" alt="Logo" className="small" /> Contact</h2>
                            <p>Email: bootproject@icloud.com</p>
                        </li>
                    </ul>
                    <div>(&copy;) Copyright www.bootproject.se (footer.se) 2018</div>
                </div>

            </section>
        </footer>
        );
}

const Intro = () => {
    return (
        <div className="sectionroot">
            <section>
                <div className="sectioncontent">
                    <div className="sharedSection">
                        <h1><span>Boot</span><span>project </span><span>-developer</span><span>connection</span></h1>
                        <div className="imagecontainer">
                            <img alt="Logo back" src="/img/logobg.svg" />
                        </div>
                        <div>
                            <p className="intro">Bootproject is development base for all projects and also where to create and test out new features from both .NET, javascript frameworks and more.<br /></p>
                            <a className="biglink" href="https://bitbucket.org/rickardmagnusson/learningreactv2/src/master/" rel="noopener noreferrer" target="_blank">Grab your copy v2 on bitbucket</a>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    );
}


export { Footer, Intro };
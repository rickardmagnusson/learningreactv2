﻿import React from "react";
import { Nav } from "../components/nav";
import { Zone } from "../plugins/zones";
import { Footer, Intro } from "../static";

const HomeTemplate = ({ match }) => {

    return (
        <section>
            <div className="container">
                <Nav title="Bootproject" />
                <Intro name="Bootproject" />
                <Zone zone="BeforeContent" match={match} />
                <Zone zone="Content" match={match} />
                <Zone zone="Timeline" match={match} />
                <Zone zone="AfterContent" match={match} />
                <Zone zone="AboutContent" match={match} />
                <Footer />
            </div>
        </section>
    );
};

const DefaultTemplate = ({ match }) => {
    return (
        <section>
            <div className="container">
                <Nav title="Bootproject" />
                <Zone zone="BeforeContent" match={match} />
                <Zone zone="Content" match={match} />
                <Zone zone="AfterContent" match={match} />
                <Zone zone="AboutContent" match={match} />
                <Footer />
            </div>
        </section>
    );
};

export { HomeTemplate, DefaultTemplate };

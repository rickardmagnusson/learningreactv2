﻿import React from 'react';
import ContentItem from "../components/contentitem";
import DefaultTemplate from "../templates/defaulttemplate";
import HomeTemplate from "../templates/hometemplate";
import Timeline from "../components/timeline";
import Topics from "../components/topics";

const Modules = {
    "ContentItem": ContentItem,
    "HomeTemplate": HomeTemplate,
    "DefaultTemplate": DefaultTemplate,
    "Timeline": Timeline,
    "Topics": Topics
}

export default Modules;